-- MySQL CRUD Operations

-- [SECTION] Inserting A Record

-- SYNTAX: INSERT INTO table_name (column_nameA) VALUES (ValueA);
-- Artist Table
INSERT INTO artists (name) VALUES ("Psy");
INSERT INTO artists (name) VALUES ("Rivermaya");

-- select specific table
SELECT * FROM artists;

INSERT INTO albums (album_title, date_released, artists_id) VALUES ("Psy 6", "2012-07-15", 1);
INSERT INTO albums (album_title, date_released, artists_id) VALUES ("Trip", "1996-02-14", 2);

-- HH:MM:SS -> HHMMSS
INSERT INTO songs (song_name, music_length, music_genre, album_id) VALUES ("Gangname Style", 339, "KPOP", 1);

INSERT INTO songs (song_name, music_length, music_genre, album_id) VALUES ("Kundiman", 524, "OPM", 2);
INSERT INTO songs (song_name, music_length, music_genre, album_id) VALUES ("Kisapmata", 441, "OPM", 2);


-- [SECTION] Selecting Records

-- SYNTAX: 
	-- SELECT * FROM table_name; (shows the full table)
		-- (*) means all columns will be sshown in the selected table.
	-- SELECT column_nameA, column_nameB FROM table_name;

SELECT * FROM songs;

-- Display the title and genre of all the songs
SELECT song_name, music_genre FROM songs;

SELECT album_title, date_released FROM albums;

-- Display a specific value
-- "WHERE" clauase is used to filter records and to extract only those records that fulfill a specific condition.

	-- display the title of all the OPM songs
	SELECT song_name FROM songs WHERE music_genre = "OPM";

-- AND & OR Keyword 
	-- is used for multiple expressions in the WHERE clauase

	-- Display the title and length of the OPM songs that are more than 4 mins 30 secs.
	SELECT song_name, music_length FROM songs WHERE music_length > 430 AND music_genre = "OPM";

-- [SECTION] Updating Records
-- SYNTAX:
	-- UPDATE table_name SET column_nameA = updated_valueA WHERE condition;

-- Update the length of Kundiman to 4 mins 24 secs.
UPDATE songs SET music_length = 424 WHERE song_name = "Kundiman";

-- [SECTION] Deleting Records
-- Syntax: 
	--DELETE FROM table_name WHERE condition;
	-- NOTE: Removing the WHERE clause will remove all rows in the table.

-- Delete all OPM songs that are more than 4 mins 30 secs.
DELETE FROM songs WHERE music_genre = "OPM" AND music_length > 430;